package com.wml.client.domain;

/**
 * 用来封装文件展示对象
 * 
 * @author macpl
 *
 */
public class FileInfo {
	// 文件对象
	private String id;
	// 扩展名
	private String exname;
	// 文件别名
	private String title;
	// 文件大小
	private Integer filesize;
	// -------------------------------
	// 下载地址
	private String downloadUrl;
	// 图标地址
	private String iconUrl;
	// 预览地址
	private String viewUrl;
	// 状态标签
	private String statelabel;
	// 模型标签
	private String modellabel;
	// 真实地址
	private String realpath;
	// 文件大小标签
	private String sizelabel;
	// 下载数量
	private int downNum;
	// 预览次数
	private int viewNum;
	private String secret;

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getSizelabel() {
		return sizelabel;
	}

	public void setSizelabel(String sizelabel) {
		this.sizelabel = sizelabel;
	}

	public String getRealpath() {
		return realpath;
	}

	public void setRealpath(String realpath) {
		this.realpath = realpath;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getExname() {
		return exname;
	}

	public void setExname(String exname) {
		this.exname = exname;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getFilesize() {
		return filesize;
	}

	public void setFilesize(Integer filesize) {
		this.filesize = filesize;
	}

	public String getStatelabel() {
		return statelabel;
	}

	public void setStatelabel(String statelabel) {
		this.statelabel = statelabel;
	}

	public String getModellabel() {
		return modellabel;
	}

	public void setModellabel(String modellabel) {
		this.modellabel = modellabel;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getViewUrl() {
		return viewUrl;
	}

	public void setViewUrl(String viewUrl) {
		this.viewUrl = viewUrl;
	}

	public int getDownNum() {
		return downNum;
	}

	public void setDownNum(int downNum) {
		this.downNum = downNum;
	}

	public int getViewNum() {
		return viewNum;
	}

	public void setViewNum(int viewNum) {
		this.viewNum = viewNum;
	}

	public String getSecret() {
		return secret;
	}
}
