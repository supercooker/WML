package com.wml.client.https;


import org.apache.commons.codec.binary.Base64;

public class PasteBase64Img {
	private byte[] data;

	public PasteBase64Img(String base64Str) {
		String[] arrImageData = base64Str.split(",");
		if (arrImageData.length > 1) {
			data = Base64.decodeBase64(arrImageData[1]);
		} else {
			data = Base64.decodeBase64(arrImageData[0]);
		}

	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	
}
