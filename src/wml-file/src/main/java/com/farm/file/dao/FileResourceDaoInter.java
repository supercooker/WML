package com.farm.file.dao;

import com.farm.file.domain.FileResource;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;



public interface FileResourceDaoInter  {
 
 public void deleteEntity(FileResource fileresource) ;
 
 public FileResource getEntity(String fileresourceid) ;
 
 public  FileResource insertEntity(FileResource fileresource);
 
 public int getAllListNum();
 
 public void editEntity(FileResource fileresource);
 
 public Session getSession();
 
 public DataResult runSqlQuery(DataQuery query);
 
 public void deleteEntitys(List<DBRule> rules);

 
 public List<FileResource> selectEntitys(List<DBRule> rules);

 
 public void updataEntitys(Map<String, Object> values, List<DBRule> rules);
 
 public int countEntitys(List<DBRule> rules);
}