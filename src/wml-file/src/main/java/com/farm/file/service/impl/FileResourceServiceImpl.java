package com.farm.file.service.impl;

import com.farm.file.domain.FileResource;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;
import com.farm.file.dao.FileResourceDaoInter;
import com.farm.file.service.FileResourceServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;


@Service
public class FileResourceServiceImpl implements FileResourceServiceInter {
	@Resource
	private FileResourceDaoInter fileresourceDaoImpl;

	private static final Logger log = Logger.getLogger(FileResourceServiceImpl.class);

	@Override
	@Transactional
	public FileResource insertFileresourceEntity(FileResource entity, LoginUser user) {
		entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setCusername(user.getName());
		entity.setEuser(user.getId());
		entity.setEusername(user.getName());
		entity.setEtime(TimeTool.getTimeDate14());
		return fileresourceDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public FileResource editFileresourceEntity(FileResource entity, LoginUser user) {
		FileResource entity2 = fileresourceDaoImpl.getEntity(entity.getId());
		entity2.setPath(entity.getPath());
		entity2.setState(entity.getState());
		entity2.setPcontent(entity.getPcontent());
		entity2.setEuser(user.getId());
		entity2.setEusername(user.getName());
		entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setTitle(entity.getTitle());
		fileresourceDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteFileresourceEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		fileresourceDaoImpl.deleteEntity(fileresourceDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public FileResource getFileresourceEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return fileresourceDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createFileresourceSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "WML_F_RESOURCE",
				"ID,PATH,STATE,PCONTENT,EUSER,EUSERNAME,TITLE,CUSER,CUSERNAME,ETIME,CTIME");
		return dbQuery;
	}

	@Override
	@Transactional
	public List<FileResource> getResources(boolean loadSpaceInfo) {
		List<FileResource> list = fileresourceDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("STATE", "0", "!=")).toList());
		if (loadSpaceInfo) {
			for (FileResource r : list) {
				String path = r.getPath();
				File[] roots = File.listRoots();
				for (File rootfile : roots) {
					if (path.indexOf(rootfile.getPath()) >= 0) {
						long usable = ((rootfile.getUsableSpace() / (1024 * 1024)) / 1024);
						long total = ((rootfile.getTotalSpace() / (1024 * 1024)) / 1024);
						if (usable != 0 && total != 0) {
							r.setSpaceinfo("可用空间" + usable + "G/总空间" + total + "G");
							r.setSpacenum((int) (usable * 100 / total));
						}
					}
				}
			}
		}
		return list;
	}

	@Override
	@Transactional
	public String getRandomResourceid() {
		List<FileResource> resources = fileresourceDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("STATE", "1", "=")).toList());
		Random random = new Random();
		if (resources.size() <= 0) {
			log.error("无可用资源库,请检查资源库数据！");
		}
		int n = random.nextInt() % resources.size();
		return resources.get(Math.abs(n)).getId();
	}

}
