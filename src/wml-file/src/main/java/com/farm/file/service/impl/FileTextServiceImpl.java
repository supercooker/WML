package com.farm.file.service.impl;

import com.farm.file.domain.FileBase;
import com.farm.file.domain.FileText;
import org.apache.log4j.Logger;

import com.farm.file.dao.FileBaseDaoInter;
import com.farm.file.dao.FileTextDaoInter;
import com.farm.file.service.FileTextServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;


@Service
public class FileTextServiceImpl implements FileTextServiceInter {
	@Resource
	private FileTextDaoInter filetextDaoImpl;
	@Resource
	private FileBaseDaoInter filebaseDaoImpl;
	private static final Logger log = Logger.getLogger(FileTextServiceImpl.class);

	@Override
	@Transactional
	public FileText insertFiletextEntity(FileText entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		entity.setCompleteis("0");
		return filetextDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public FileText editFiletextEntity(FileText entity, LoginUser user) {
		FileText entity2 = filetextDaoImpl.getEntity(entity.getId());
		entity2.setFileid(entity.getFileid());
		int MAXLENGTH = 2000000;
		if (entity.getFiletext().length() >= MAXLENGTH) {
			entity.setFiletext(entity.getFiletext().substring(0, MAXLENGTH - 1));
		}
		entity2.setFiletext(entity.getFiletext());
		entity2.setPcontent(entity.getPcontent());
		entity2.setPstate(entity.getPstate());
		entity2.setCtime(entity.getCtime());
		entity2.setId(entity.getId());
		entity2.setCompleteis(entity.getCompleteis());
		filetextDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	public FileText editFiletextEntity(FileText filetext) {
		return editFiletextEntity(filetext, null);
	}

	@Override
	@Transactional
	public void deleteFiletextEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		filetextDaoImpl.deleteEntity(filetextDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public FileText getFiletextEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return filetextDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createFiletextSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "wml_f_text", "ID,FILEID,FILETEXT,PCONTENT,PSTATE,CTIME");
		return dbQuery;
	}

	@Override
	@Transactional
	public void insertFiletextEntity(FileBase filebase, LoginUser user) {
		FileText text = new FileText();
		text.setFileid(filebase.getId());
		text.setCtime(filebase.getCtime());
		text.setFiletext(filebase.getTitle());
		text.setPstate("1");
		insertFiletextEntity(text, user);
	}

	@Override
	@Transactional
	public FileText getFiletextByFileId(String fileid) {
		List<FileText> texts = filetextDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("FILEID", fileid, "=")).toList());
		if (texts.size() <= 0) {
			return null;
		}
		return texts.get(0);
	}

	@Override
	@Transactional
	public void deleteFiletextByFileid(String fileid, LoginUser user) {
		filetextDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("FILEID", fileid, "=")).toList());
	}

	@Override
	@Transactional
	public void clearFiletextByFileid(String fileid, LoginUser loginUser) {
		FileText text = getFiletextByFileId(fileid);
		if (text == null) {
			insertFiletextEntity(filebaseDaoImpl.getEntity(fileid), loginUser);
		} else {
			text.setFiletext("");
			text.setCompleteis("0"); 
			filetextDaoImpl.editEntity(text);
		}
	}

}
