package com.farm.file.service.impl;

import com.farm.file.domain.FileBase;
import com.farm.file.domain.Visit;
import com.farm.file.domain.Visitlogs;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;
import com.farm.file.dao.VisitDaoInter;
import com.farm.file.dao.VisitlogsDaoInter;
import com.farm.file.service.VisitServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;


@Service
public class VisitServiceImpl implements VisitServiceInter {
	@Resource
	private VisitDaoInter filevisitDaoImpl;
	@Resource
	private VisitlogsDaoInter visitlogsDaoImpl;
	private static final Logger log = Logger.getLogger(VisitServiceImpl.class);

	@Override
	@Transactional
	public Visit insertFilevisitEntity(Visit entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return filevisitDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public Visit editFilevisitEntity(Visit entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		Visit entity2 = filevisitDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setAppid(entity.getAppid());
		entity2.setViewnum(entity.getViewnum());
		entity2.setDownum(entity.getDownum());
		entity2.setId(entity.getId());
		filevisitDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteFilevisitEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		filevisitDaoImpl.deleteEntity(filevisitDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public Visit getFilevisitEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return filevisitDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createFilevisitSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "WML_F_VISIT", ",APPID,VIEWNUM,DOWNUM,ID");
		return dbQuery;
	}

	@Override
	@Transactional
	public void insertFilevisitEntity(FileBase filebase, LoginUser user) {
		Visit visit = new Visit();
		visit.setDownum(0);
		visit.setAppid(filebase.getId());
		visit.setViewnum(0);
		insertFilevisitEntity(visit, user);
	}

	@Override
	@Transactional
	public Visit getVisitByAppId(String appid) {
		List<Visit> texts = filevisitDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("APPID", appid, "=")).toList());
		if (texts.size() <= 0) {
			Visit visit = new Visit();
			visit.setDownum(0);
			visit.setAppid(appid);
			visit.setViewnum(0);
			filevisitDaoImpl.insertEntity(visit);
			return visit;
		}
		return texts.get(0);
	}

	@Override
	@Transactional
	public void deleteVisitByAppid(String appid, LoginUser user) {
		filevisitDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("APPID", appid, "=")).toList());
	}

	@Override
	@Transactional
	public Visit record(String appid, OpModel op, LoginUser user, int val1, String val2) {
		Visit visit = getVisitByAppId(appid);
		// VISIT("3", "访问"),
		if (op.equals(OpModel.VISIT)) {
			visit.setVisit(visit.getVisit() == null ? 1 : visit.getVisit() + 1);
		}
		// DOWN("1", "下载"),
		if (op.equals(OpModel.DOWN)) {
			visit.setDownum(visit.getDownum() == null ? 1 : visit.getDownum() + 1);
		}
		// VIEW("2", "预览"),
		if (op.equals(OpModel.VIEW)) {
			visit.setViewnum(visit.getViewnum() == null ? 1 : visit.getViewnum() + 1);
		}
		// PRAISE("4", "评价"),
		if (op.equals(OpModel.PRAISE)) {
			// visit.setPraise(visit.getPraise() == null ? 1 : visit.getPraise() + 1);
		}
		// COMMENTS("5", "评论"),
		if (op.equals(OpModel.COMMENTS)) {
			visit.setComments(visit.getComments() == null ? 1 : visit.getComments() + 1);
		}
		// FAVORITES("6", "收藏")
		if (op.equals(OpModel.FAVORITES)) {
			visit.setFavorites(visit.getFavorites() == null ? 1 : visit.getFavorites() + 1);
		}
		// FAVORITES("7", "收藏")
		if (op.equals(OpModel.BOOK)) {
			// visit.setFavorites(visit.getFavorites() == null ? 1 : visit.getFavorites() +
			// 1);
		}
		// -----------------------------------------------------------------------------------------------------------
		// VISIT("3", "访问"),DOWN("1", "下载"),VIEW("2", "预览"),
		if (op.equals(OpModel.VISIT) || op.equals(OpModel.DOWN) || op.equals(OpModel.VIEW)) {
			visitlogsDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("TYPE", op.getType(), "="))
					.add(new DBRule("CUSER", user.getId(), "=")).add(new DBRule("APPID", appid, "=")).toList());
			Visitlogs log = new Visitlogs();
			log.setCtime(TimeTool.getTimeDate14());
			log.setCuser(user != null ? user.getId() : "NONE");
			log.setAppid(appid);
			log.setCusername(user.getName());
			// log.setIntflag(null);
			log.setType(op.getType());
			visitlogsDaoImpl.insertEntity(log);
		}

		// PRAISE("4", "评价"),
		if (op.equals(OpModel.PRAISE) && user != null && val1 != 0) {
			visitlogsDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("TYPE", op.getType(), "="))
					.add(new DBRule("CUSER", user.getId(), "=")).add(new DBRule("APPID", appid, "=")).toList());
			Visitlogs log = new Visitlogs();
			log.setCtime(TimeTool.getTimeDate14());
			log.setCuser(user != null ? user.getId() : "NONE");
			log.setAppid(appid);
			log.setCusername(user.getName());
			log.setIntflag(val1);
			log.setType(op.getType());
			visitlogsDaoImpl.insertEntity(log);
			int num = visitlogsDaoImpl.countEntitys(DBRuleList.getInstance().add(new DBRule("TYPE", op.getType(), "="))
					.add(new DBRule("APPID", appid, "=")).toList());
			int sum = visitlogsDaoImpl.sumEntitys(DBRuleList.getInstance().add(new DBRule("TYPE", op.getType(), "="))
					.add(new DBRule("APPID", appid, "=")).toList());
			num = num + 1;
			sum = sum + val1;
			visit.setPraise((int) sum / num);
		}
		// COMMENTS("5", "评论"),
		if (op.equals(OpModel.COMMENTS)) {
			Visitlogs log = new Visitlogs();
			log.setCtime(TimeTool.getTimeDate14());
			log.setCuser(user != null ? user.getId() : "NONE");
			log.setCusername(user.getName());
			log.setAppid(appid);
			log.setStrflag(val2);
			log.setType(op.getType());
			visitlogsDaoImpl.insertEntity(log);
			visit.setLog(log);
		}
		// FAVORITES("6", "收藏")
		if (op.equals(OpModel.FAVORITES) || op.equals(OpModel.BOOK)) {
			visitlogsDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("TYPE", op.getType(), "="))
					.add(new DBRule("CUSER", user.getId(), "=")).add(new DBRule("APPID", appid, "=")).toList());
			Visitlogs log = new Visitlogs();
			log.setCtime(TimeTool.getTimeDate14());
			log.setCuser(user != null ? user.getId() : "NONE");
			log.setAppid(appid);
			log.setCusername(user.getName());
			log.setType(op.getType());
			visitlogsDaoImpl.insertEntity(log);
		}
		filevisitDaoImpl.editEntity(visit);
		return visit;
	}

	@Override
	@Transactional
	public Visit record(String appid, OpModel visit, LoginUser user) {
		return record(appid, visit, user, 0, null);
	}

	@Override
	@Transactional
	public Visitlogs record(String appid, OpModel visit, LoginUser user, String val) {
		return record(appid, visit, user, 0, val).getLog();
	}

	@Override
	@Transactional
	public Visit record(String appid, OpModel visit, LoginUser user, int val) {
		return record(appid, visit, user, val, null);
	}

	@Override
	@Transactional
	public Visitlogs getRecordLog(String appid, OpModel op, LoginUser user) {
		if (user != null) {
			List<Visitlogs> list = visitlogsDaoImpl
					.selectEntitys(DBRuleList.getInstance().add(new DBRule("TYPE", op.getType(), "="))
							.add(new DBRule("CUSER", user.getId(), "=")).add(new DBRule("APPID", appid, "=")).toList());
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
		}
		return null;
	}

	@Override
	@Transactional
	public List<Visitlogs> getRecordLogs(String appid, OpModel op) {
		List<Visitlogs> list = visitlogsDaoImpl.selectEntitys(DBRuleList.getInstance()
				.add(new DBRule("TYPE", op.getType(), "=")).add(new DBRule("APPID", appid, "=")).toList());
		Collections.sort(list, (o1, o2) -> o2.getCtime().compareTo(o1.getCtime()));
		return list;
	}

	@Override
	@Transactional
	public int getUserLogsNum(LoginUser user, OpModel opmodel) {
		int num = visitlogsDaoImpl.countEntitys(DBRuleList.getInstance().add(new DBRule("TYPE", opmodel.getType(), "="))
				.add(new DBRule("CUSER", user.getId(), "=")).toList());
		return num;
	}

	@Override
	@Transactional
	public boolean hasVisitLog(String appid, OpModel op, LoginUser currentUser) {
		int num = visitlogsDaoImpl.countEntitys(DBRuleList.getInstance().add(new DBRule("TYPE", op.getType(), "="))
				.add(new DBRule("APPID", appid, "=")).add(new DBRule("CUSER", currentUser.getId(), "=")).toList());
		return num > 0;
	}

}
