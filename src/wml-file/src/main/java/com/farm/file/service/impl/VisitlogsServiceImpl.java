package com.farm.file.service.impl;

import com.farm.file.domain.Visitlogs;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;
import com.farm.file.dao.VisitlogsDaoInter;
import com.farm.file.service.VisitlogsServiceInter;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

@Service
public class VisitlogsServiceImpl implements VisitlogsServiceInter{
  @Resource
  private VisitlogsDaoInter  visitlogsDaoImpl;

  private static final Logger log = Logger.getLogger(VisitlogsServiceImpl.class);
  @Override
  @Transactional
  public Visitlogs insertVisitlogsEntity(Visitlogs entity,LoginUser user) {
    // TODO 自动生成代码,修改后请去除本注释
    //entity.setCuser(user.getId());
    //entity.setCtime(TimeTool.getTimeDate14());
    //entity.setCusername(user.getName());
    //entity.setEuser(user.getId()); 
    //entity.setEusername(user.getName());
    //entity.setEtime(TimeTool.getTimeDate14());
    //entity.setPstate("1");
    return visitlogsDaoImpl.insertEntity(entity);
  }
  @Override
  @Transactional
  public Visitlogs editVisitlogsEntity(Visitlogs entity,LoginUser user) {
    // TODO 自动生成代码,修改后请去除本注释
    Visitlogs entity2 = visitlogsDaoImpl.getEntity(entity.getId());
    //entity2.setEuser(user.getId());
    //entity2.setEusername(user.getName());
    //entity2.setEtime(TimeTool.getTimeDate14()); 
    entity2.setStrflag(entity.getStrflag());
    entity2.setIntflag(entity.getIntflag());
    entity2.setType(entity.getType());
    entity2.setCuser(entity.getCuser());
    entity2.setCtime(entity.getCtime());
    entity2.setId(entity.getId());
    visitlogsDaoImpl.editEntity(entity2);
    return entity2;
  }
  @Override
  @Transactional
  public void deleteVisitlogsEntity(String id,LoginUser user) {
    // TODO 自动生成代码,修改后请去除本注释
    visitlogsDaoImpl.deleteEntity(visitlogsDaoImpl.getEntity(id));
  }
  @Override
  @Transactional
  public Visitlogs getVisitlogsEntity(String id) {
    // TODO 自动生成代码,修改后请去除本注释
    if (id == null){return null;}
    return visitlogsDaoImpl.getEntity(id);
  }
  @Override
  @Transactional
  public DataQuery createVisitlogsSimpleQuery(DataQuery query) {
    // TODO 自动生成代码,修改后请去除本注释
    DataQuery dbQuery = DataQuery
        .init(
            query,
            "WML_F_VISITLOGS",
            "ID,STRFLAG,INTFLAG,TYPE,CUSER,CTIME");
    return dbQuery;
  }

}
