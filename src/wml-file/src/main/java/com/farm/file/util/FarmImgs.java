package com.farm.file.util;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.imageio.ImageIO;

import com.farm.file.util.domain.ColorBean;

import net.coobird.thumbnailator.Thumbnails;

public class FarmImgs {
	
	public static String getFileWidthAndHeight(File file) throws IOException {
		final BufferedImage image = ImageIO.read(file);
		final int width = image.getWidth();
		final int height = image.getHeight();
		return width + ":" + height;
	}

	public static List<ColorBean> getColorList() {
		List<ColorBean> result = new ArrayList<ColorBean>();
		result.add(new ColorBean("白色", "#ffffff"));
		result.add(new ColorBean("象牙色", "#fffff0"));
		result.add(new ColorBean("亮黄色", "#ffffe0"));
		result.add(new ColorBean("黄色", "#ffff00"));
		result.add(new ColorBean("雪白色", "#fffafa"));
		result.add(new ColorBean("花白色", "#fffaf0"));
		result.add(new ColorBean("柠檬绸色", "#fffacd"));
		result.add(new ColorBean("米绸色", "#fff8dc"));
		result.add(new ColorBean("海贝色", "#fff5ee"));
		result.add(new ColorBean("淡紫红", "#fff0f5"));
		result.add(new ColorBean("番木色", "#ffefd5"));
		result.add(new ColorBean("白杏色", "#ffebcd"));
		result.add(new ColorBean("浅玫瑰色", "#ffe4e1"));
		result.add(new ColorBean("桔黄色", "#ffe4c4"));
		result.add(new ColorBean("鹿皮色", "#ffe4b5"));
		result.add(new ColorBean("纳瓦白", "#ffdead"));
		result.add(new ColorBean("桃色", "#ffdab9"));
		result.add(new ColorBean("金色", "#ffd700"));
		result.add(new ColorBean("粉红色", "#ffc0cb"));
		result.add(new ColorBean("亮粉红色", "#ffb6c1"));
		result.add(new ColorBean("橙色", "#ffa500"));
		result.add(new ColorBean("亮肉色", "#ffa07a"));
		result.add(new ColorBean("暗桔黄色", "#ff8c00"));
		result.add(new ColorBean("珊瑚色", "#ff7f50"));
		result.add(new ColorBean("热粉红色", "#ff69b4"));
		result.add(new ColorBean("西红柿色", "#ff6347"));
		result.add(new ColorBean("红橙色", "#ff4500"));
		result.add(new ColorBean("深粉红色", "#ff1493"));
		result.add(new ColorBean("紫红色", "#ff00ff"));
		result.add(new ColorBean("红紫色", "#ff00ff"));
		result.add(new ColorBean("红色", "#ff0000"));
		result.add(new ColorBean("老花色", "#fdf5e6"));
		result.add(new ColorBean("亮金黄色", "#fafad2"));
		result.add(new ColorBean("亚麻色", "#faf0e6"));
		result.add(new ColorBean("古董白", "#faebd7"));
		result.add(new ColorBean("鲜肉色", "#fa8072"));
		result.add(new ColorBean("幽灵白", "#f8f8ff"));
		result.add(new ColorBean("薄荷色", "#f5fffa"));
		result.add(new ColorBean("烟白色", "#f5f5f5"));
		result.add(new ColorBean("米色", "#f5f5dc"));
		result.add(new ColorBean("浅黄色", "#f5deb3"));
		result.add(new ColorBean("沙褐色", "#f4a460"));
		result.add(new ColorBean("天蓝色", "#f0ffff"));
		result.add(new ColorBean("蜜色", "#f0fff0"));
		result.add(new ColorBean("艾利斯兰", "#f0f8ff"));
		result.add(new ColorBean("黄褐色", "#f0e68c"));
		result.add(new ColorBean("亮珊瑚色", "#f08080"));
		result.add(new ColorBean("苍麒麟色", "#eee8aa"));
		result.add(new ColorBean("紫罗兰色", "#ee82ee"));
		result.add(new ColorBean("暗肉色", "#e9967a"));
		result.add(new ColorBean("淡紫色", "#e6e6fa"));
		result.add(new ColorBean("亮青色", "#e0ffff"));
		result.add(new ColorBean("实木色", "#deb887"));
		result.add(new ColorBean("洋李色", "#dda0dd"));
		result.add(new ColorBean("淡灰色", "#dcdcdc"));
		result.add(new ColorBean("暗深红色", "#dc143c"));
		result.add(new ColorBean("苍紫罗兰色", "#db7093"));
		result.add(new ColorBean("金麒麟色", "#daa520"));
		result.add(new ColorBean("淡紫色", "#da70d6"));
		result.add(new ColorBean("蓟色", "#d8bfd8"));
		result.add(new ColorBean("亮灰色", "#d3d3d3"));
		result.add(new ColorBean("茶色", "#d2b48c"));
		result.add(new ColorBean("巧可力色", "#d2691e"));
		result.add(new ColorBean("秘鲁色", "#cd853f"));
		result.add(new ColorBean("印第安红", "#cd5c5c"));
		result.add(new ColorBean("中紫罗兰色", "#c71585"));
		result.add(new ColorBean("银色", "#c0c0c0"));
		result.add(new ColorBean("暗黄褐色", "#bdb76b"));
		result.add(new ColorBean("褐玫瑰红", "#bc8f8f"));
		result.add(new ColorBean("中粉紫色", "#ba55d3"));
		result.add(new ColorBean("暗金黄色", "#b8860b"));
		result.add(new ColorBean("火砖色", "#b22222"));
		result.add(new ColorBean("粉蓝色", "#b0e0e6"));
		result.add(new ColorBean("亮钢兰色", "#b0c4de"));
		result.add(new ColorBean("苍宝石绿", "#afeeee"));
		result.add(new ColorBean("黄绿色", "#adff2f"));
		result.add(new ColorBean("亮蓝色", "#add8e6"));
		result.add(new ColorBean("褐色", "#a52a2a"));
		result.add(new ColorBean("赭色", "#a0522d"));
		result.add(new ColorBean("暗紫色", "#9932cc"));
		result.add(new ColorBean("苍绿色", "#98fb98"));
		result.add(new ColorBean("暗紫罗兰色", "#9400d3"));
		result.add(new ColorBean("中紫色", "#9370db"));
		result.add(new ColorBean("亮绿色", "#90ee90"));
		result.add(new ColorBean("暗海兰色", "#8fbc8f"));
		result.add(new ColorBean("重褐色", "#8b4513"));
		result.add(new ColorBean("暗洋红", "#8b008b"));
		result.add(new ColorBean("暗红色", "#8b0000"));
		result.add(new ColorBean("紫罗兰蓝色", "#8a2be2"));
		result.add(new ColorBean("亮天蓝色", "#87cefa"));
		result.add(new ColorBean("天蓝色", "#87ceeb"));
		result.add(new ColorBean("灰色", "#808080"));
		result.add(new ColorBean("橄榄色", "#808000"));
		result.add(new ColorBean("紫色", "#800080"));
		result.add(new ColorBean("粟色", "#800000"));
		result.add(new ColorBean("碧绿色", "#7fffd4"));
		result.add(new ColorBean("黄绿色", "#7fff00"));
		result.add(new ColorBean("草绿色", "#7cfc00"));
		result.add(new ColorBean("中暗蓝色", "#7b68ee"));
		result.add(new ColorBean("亮蓝灰", "#778899"));
		result.add(new ColorBean("灰石色", "#708090"));
		result.add(new ColorBean("深绿褐色", "#6b8e23"));
		result.add(new ColorBean("石蓝色", "#6a5acd"));
		result.add(new ColorBean("暗灰色", "#696969"));
		result.add(new ColorBean("中绿色", "#66cdaa"));
		result.add(new ColorBean("菊兰色", "#6495ed"));
		result.add(new ColorBean("军兰色", "#5f9ea0"));
		result.add(new ColorBean("暗橄榄绿", "#556b2f"));
		result.add(new ColorBean("靛青色", "#4b0082"));
		result.add(new ColorBean("中绿宝石", "#48d1cc"));
		result.add(new ColorBean("暗灰蓝色", "#483d8b"));
		result.add(new ColorBean("钢兰色", "#4682b4"));
		result.add(new ColorBean("皇家蓝", "#4169e1"));
		result.add(new ColorBean("青绿色", "#40e0d0"));
		result.add(new ColorBean("中海蓝", "#3cb371"));
		result.add(new ColorBean("橙绿色", "#32cd32"));
		result.add(new ColorBean("暗瓦灰色", "#2f4f4f"));
		result.add(new ColorBean("海绿色", "#2e8b57"));
		result.add(new ColorBean("森林绿", "#228b22"));
		result.add(new ColorBean("亮海蓝色", "#20b2aa"));
		result.add(new ColorBean("闪兰色", "#1e90ff"));
		result.add(new ColorBean("中灰兰色", "#191970"));
		result.add(new ColorBean("浅绿色", "#00ffff"));
		result.add(new ColorBean("青色", "#00ffff"));
		result.add(new ColorBean("春绿色", "#00ff7f"));
		result.add(new ColorBean("酸橙色", "#00ff00"));
		result.add(new ColorBean("中春绿色", "#00fa9a"));
		result.add(new ColorBean("暗宝石绿", "#00ced1"));
		result.add(new ColorBean("深天蓝色", "#00bfff"));
		result.add(new ColorBean("暗青色", "#008b8b"));
		result.add(new ColorBean("水鸭色", "#008080"));
		result.add(new ColorBean("绿色", "#008000"));
		result.add(new ColorBean("暗绿色", "#006400"));
		result.add(new ColorBean("蓝色", "#0000ff"));
		result.add(new ColorBean("中兰色", "#0000cd"));
		result.add(new ColorBean("暗蓝色", "#00008b"));
		result.add(new ColorBean("海军色", "#000080"));
		result.add(new ColorBean("黑色", "#000000"));
		return result;
	}

	
	public static ColorBean getColor(int r, int g, int b) {
		ColorBean colorb = null;
		int minRGB = 765;
		ColorBean minColorBean = null;
		for (ColorBean cb : getColorList()) {
			int tempR = Math.abs(r - cb.getR());
			int tempG = Math.abs(g - cb.getG());
			int tempB = Math.abs(b - cb.getB());
			int tempRGB = (tempR + tempG + tempB) / 3;
			if (tempRGB <= minRGB) {
				minRGB = tempRGB;
				minColorBean = cb;
			}
			colorb = minColorBean;
		}
		return colorb;
	}

	public static List<ColorBean> getColors(File file, int num) throws IOException {
		BufferedImage imge = Thumbnails.of(file).width(16).asBufferedImage();
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (int w = 0; w < imge.getWidth(); w++) {
			for (int h = 0; h < imge.getHeight(); h++) {
				Object data = imge.getRaster().getDataElements(w, h, null);
				int r = imge.getColorModel().getRed(data);
				int g = imge.getColorModel().getGreen(data);
				int b = imge.getColorModel().getBlue(data);
				String h16 = FarmImgs.getColor(r, g, b).getColorHex();
				if (map.containsKey(h16)) {
					map.put(h16, map.get(h16) + 1);
				} else {
					map.put(h16, 0);
				}
			}
		}
		List<Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>();
		list.addAll(map.entrySet());
		Collections.sort(list, new Comparator<Entry<String, Integer>>() {
			@Override
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
				return o2.getValue() - o1.getValue();
			}
		});
		List<ColorBean> colorlist = new ArrayList<ColorBean>();
		for (Entry<String, Integer> node : list) {
			for (ColorBean bean : getColorList()) {
				if (colorlist.size() >= num) {
					break;
				}
				if (node.getKey().equals(bean.getColorHex())) {
					colorlist.add(bean);
				}
			}
		}
		return colorlist;
	}
}
