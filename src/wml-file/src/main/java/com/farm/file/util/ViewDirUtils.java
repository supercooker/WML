package com.farm.file.util;

import java.io.File;


public class ViewDirUtils {

	
	public static String getPath(File realfile) {
		String path = realfile.getParentFile().getPath();
		path = path + File.separator + realfile.getName().replaceAll("\\.", "_") + "_view";
		return path;
	}

}
