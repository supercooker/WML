package com.farm.material.dao;

import com.farm.material.domain.Categraytag;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;



public interface CategraytagDaoInter  {
 
 public void deleteEntity(Categraytag categraytag) ;
 
 public Categraytag getEntity(String categraytagid) ;
 
 public  Categraytag insertEntity(Categraytag categraytag);
 
 public int getAllListNum();
 
 public void editEntity(Categraytag categraytag);
 
 public Session getSession();
 
 public DataResult runSqlQuery(DataQuery query);
 
 public void deleteEntitys(List<DBRule> rules);

 
 public List<Categraytag> selectEntitys(List<DBRule> rules);

 
 public void updataEntitys(Map<String, Object> values, List<DBRule> rules);
 
 public int countEntitys(List<DBRule> rules);
}