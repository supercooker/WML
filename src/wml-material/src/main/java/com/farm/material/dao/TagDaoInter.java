package com.farm.material.dao;

import com.farm.material.domain.Tag;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;



public interface TagDaoInter  {
 
 public void deleteEntity(Tag tag) ;
 
 public Tag getEntity(String tagid) ;
 
 public  Tag insertEntity(Tag tag);
 
 public int getAllListNum();
 
 public void editEntity(Tag tag);
 
 public Session getSession();
 
 public DataResult runSqlQuery(DataQuery query);
 
 public void deleteEntitys(List<DBRule> rules);

 
 public List<Tag> selectEntitys(List<DBRule> rules);

 
 public void updataEntitys(Map<String, Object> values, List<DBRule> rules);
 
 public int countEntitys(List<DBRule> rules);
}