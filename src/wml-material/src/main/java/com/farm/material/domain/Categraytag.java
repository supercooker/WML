package com.farm.material.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "CategrayTag")
@Table(name = "wml_c_categraytag")
public class Categraytag implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "CNUM", length = 10)
        private Integer cnum;
        @Column(name = "CATEGRAYID", length = 32, nullable = false)
        private String categrayid;
        @Column(name = "TAGID", length = 32, nullable = false)
        private String tagid;

        public Integer  getCnum() {
          return this.cnum;
        }
        public void setCnum(Integer cnum) {
          this.cnum = cnum;
        }
        public String  getCategrayid() {
          return this.categrayid;
        }
        public void setCategrayid(String categrayid) {
          this.categrayid = categrayid;
        }
        public String  getTagid() {
          return this.tagid;
        }
        public void setTagid(String tagid) {
          this.tagid = tagid;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}