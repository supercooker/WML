package com.farm.material.service.impl;

import com.farm.material.domain.Categrayfile;
import com.farm.core.time.TimeTool;
import com.farm.file.FarmFileServiceInter;
import com.farm.file.impl.FarmFileServiceImpl;
import com.farm.file.service.FileBaseServiceInter;

import org.apache.log4j.Logger;
import com.farm.material.dao.CategrayfileDaoInter;
import com.farm.material.service.CategrayfileServiceInter;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;


@Service
public class CategrayfileServiceImpl implements CategrayfileServiceInter {
	@Resource
	private CategrayfileDaoInter categrayfileDaoImpl;
	@Resource
	private FarmFileServiceInter farmFileServiceImpl;
	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;
	private static final Logger log = Logger.getLogger(CategrayfileServiceImpl.class);

	@Override
	@Transactional
	public Categrayfile insertCategrayfileEntity(Categrayfile entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return categrayfileDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public Categrayfile editCategrayfileEntity(Categrayfile entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		Categrayfile entity2 = categrayfileDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setCategrayid(entity.getCategrayid());
		entity2.setFileid(entity.getFileid());
		entity2.setId(entity.getId());
		categrayfileDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteCategrayfileEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		categrayfileDaoImpl.deleteEntity(categrayfileDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public Categrayfile getCategrayfileEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return categrayfileDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createCategrayfileSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query,
				"WML_C_CATEGRAYFILE A LEFT JOIN WML_F_FILE B ON B.ID=A.FILEID LEFT JOIN WML_C_CATEGRAY C ON C.ID=A.CATEGRAYID",
				"B.TITLE AS FILENAME,B.ID AS FILEID,C.NAME AS CATEGRAYNAME,C.ID AS CATEGRAYID,A.ID AS ID");
		return dbQuery;
	}

	@Override
	@Transactional
	public void delByLogic(String categrayFileId, LoginUser user) {
		Categrayfile cf = categrayfileDaoImpl.getEntity(categrayFileId);
		cf.setPstate("0");
		cf.setDuser(user.getId());
		categrayfileDaoImpl.editEntity(cf);
	}

	@Override
	@Transactional
	public void restoreFile(String categrayFileId, LoginUser currentUser) {
		Categrayfile cf = categrayfileDaoImpl.getEntity(categrayFileId);
		cf.setPstate("1");
		categrayfileDaoImpl.editEntity(cf);
	}

	@Override
	@Transactional
	public void physicsDelFile(String categrayFileId, LoginUser currentUser) {
		Categrayfile file = categrayfileDaoImpl.getEntity(categrayFileId);
		fileBaseServiceImpl.deleteFileByLogic(file.getFileid(), currentUser);
		fileBaseServiceImpl.deleteFilebaseEntity(file.getFileid(), currentUser);
		categrayfileDaoImpl.deleteEntity(file);
	}

}
