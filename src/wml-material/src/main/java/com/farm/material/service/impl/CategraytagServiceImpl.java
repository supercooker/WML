package com.farm.material.service.impl;

import com.farm.material.domain.Categraytag;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;
import com.farm.material.dao.CategraytagDaoInter;
import com.farm.material.service.CategraytagServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;


@Service
public class CategraytagServiceImpl implements CategraytagServiceInter {
	@Resource
	private CategraytagDaoInter categraytagDaoImpl;

	private static final Logger log = Logger.getLogger(CategraytagServiceImpl.class);

	@Override
	@Transactional
	public Categraytag insertCategraytagEntity(Categraytag entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return categraytagDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public Categraytag editCategraytagEntity(Categraytag entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		Categraytag entity2 = categraytagDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setCnum(entity.getCnum());
		entity2.setCategrayid(entity.getCategrayid());
		entity2.setTagid(entity.getTagid());
		entity2.setId(entity.getId());
		categraytagDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteCategraytagEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		categraytagDaoImpl.deleteEntity(categraytagDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public Categraytag getCategraytagEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return categraytagDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createCategraytagSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query,
				"WML_C_CATEGRAYTAG a left join WML_C_CATEGRAY b on a.CATEGRAYID=b.id left join WML_C_TAG c on c.id=a.tagid",
				"A.ID as ID,A.CNUM AS CNUM,B.NAME AS CATEGRAYNAME,C.NAME AS TAGNAME,TAGID");
		return dbQuery;
	}

	@Override
	@Transactional
	public void dobind(String tagid, String categrayid) {
		categraytagDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("TAGID", tagid, "="))
				.add(new DBRule("CATEGRAYID", categrayid, "=")).toList());
		Categraytag entity = new Categraytag();
		entity.setCategrayid(categrayid);
		entity.setTagid(tagid);
		entity.setCnum(0);
		categraytagDaoImpl.insertEntity(entity);
	}

}
