package com.farm.wcp.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.farm.authority.service.OrganizationServiceInter;
import com.farm.authority.service.UserServiceInter;
import com.farm.core.page.ViewMode;
import com.farm.file.FarmFileServiceInter;
import com.farm.file.domain.Visitlogs;
import com.farm.file.service.FileResourceServiceInter;
import com.farm.file.service.VisitServiceInter;
import com.farm.file.service.VisitlogsServiceInter;
import com.farm.file.service.VisitServiceInter.OpModel;
import com.farm.material.domain.Categrayfile;
import com.farm.material.service.CategrayServiceInter;
import com.farm.material.service.CategrayfileServiceInter;
import com.farm.material.service.TagServiceInter;
import com.farm.util.web.HtmlUtils;
import com.farm.web.WebUtils;


@RequestMapping("/opfile")
@Controller
public class UIFileController extends WebUtils {
	private static final Logger log = Logger.getLogger(UIFileController.class);
	@Resource
	private UserServiceInter userServiceImpl;
	@Resource
	private OrganizationServiceInter organizationServiceImpl;
	@Resource
	private FarmFileServiceInter farmFileServiceImpl;
	@Resource
	private CategrayServiceInter categrayServiceImpl;
	@Resource
	private TagServiceInter tagServiceImpl;
	@Resource
	private FileResourceServiceInter fileResourceServiceImpl;
	@Resource
	private CategrayfileServiceInter categrayFileServiceImpl;
	@Resource
	private VisitServiceInter visitServiceImpl;
	@Resource
	private VisitlogsServiceInter visitLogsServiceImpl;

	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> del(String categrayFileId, HttpSession session) {
		try {
			for (String cfid : WebUtils.parseIds(categrayFileId)) {
				categrayFileServiceImpl.delByLogic(cfid, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/favorites")
	@ResponseBody
	public Map<String, Object> favorites(String categrayFileId, HttpSession session) {
		try {
			Categrayfile cfile = categrayFileServiceImpl.getCategrayfileEntity(categrayFileId);
			visitServiceImpl.record(cfile.getFileid(), OpModel.FAVORITES, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/book")
	@ResponseBody
	public Map<String, Object> book(String categrayId, HttpSession session) {
		try {
			visitServiceImpl.record(categrayId, OpModel.BOOK, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/move")
	@ResponseBody
	public Map<String, Object> move(String categrayFileId, String categrayId, HttpSession session) {
		try {
			for (String cfid : WebUtils.parseIds(categrayFileId)) {
				Categrayfile cfile = categrayFileServiceImpl.getCategrayfileEntity(cfid);
				categrayServiceImpl.bindFile(categrayId, cfile.getFileid());
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/praise")
	@ResponseBody
	public Map<String, Object> praise(String appId, Integer val, HttpSession session) {
		try {
			if (val != null && val > 7) {
				val = 7;
			}
			visitServiceImpl.record(appId, OpModel.PRAISE, getCurrentUser(session), val);
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/comments")
	@ResponseBody
	public Map<String, Object> comments(String appId, String msg, HttpSession session) {
		try {
			msg = HtmlUtils.HtmlRemoveTag(msg);
			Visitlogs vlog = visitServiceImpl.record(appId, OpModel.COMMENTS, getCurrentUser(session), msg);
			return ViewMode.getInstance().putAttr("data", vlog).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/delcomments")
	@ResponseBody
	public Map<String, Object> delcomments(String logid, HttpSession session) {
		try {
			visitLogsServiceImpl.deleteVisitlogsEntity(logid, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/delvlog")
	@ResponseBody
	public Map<String, Object> unfavorites(String vlogid, HttpSession session) {
		try {
			visitLogsServiceImpl.deleteVisitlogsEntity(vlogid, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/vlognum")
	@ResponseBody
	public Map<String, Object> vlognum(String opname, HttpSession session) {
		try {
			int num = visitServiceImpl.getUserLogsNum(getCurrentUser(session), OpModel.valueOf(opname));
			return ViewMode.getInstance().putAttr("num", num).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/redel")
	@ResponseBody
	public Map<String, Object> redel(String categrayFileId, HttpSession session) {
		try {
			categrayFileServiceImpl.restoreFile(categrayFileId, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/phdel")
	@ResponseBody
	public Map<String, Object> phdel(String categrayFileId, HttpSession session) {
		try {
			categrayFileServiceImpl.physicsDelFile(categrayFileId, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
}
