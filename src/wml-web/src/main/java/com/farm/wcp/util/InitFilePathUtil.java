package com.farm.wcp.util;

import java.io.File;
import java.util.List;

import com.farm.authority.FarmAuthorityService;
import com.farm.core.auth.domain.LoginUser;
import com.farm.file.domain.FileResource;
import com.farm.file.service.FileResourceServiceInter;
import com.farm.parameter.FarmParameterService;
import com.farm.parameter.service.ParameterServiceInter;
import com.farm.util.spring.BeanFactory;

public class InitFilePathUtil {

	public static void iniPaths() {
		ParameterServiceInter parameterServiceImpl = (ParameterServiceInter) BeanFactory
				.getBean("parameterServiceImpl");
		FileResourceServiceInter fileResourceServiceImpl = (FileResourceServiceInter) BeanFactory
				.getBean("fileResourceServiceImpl");
		LoginUser user = FarmAuthorityService.getInstance().getUserByLoginName("sysadmin");
		List<FileResource> resources = fileResourceServiceImpl.getResources(false);
		for (FileResource resource : resources) {
			if (resource.getPath().trim().equals("INIT")) {
				System.out.println(resource.getTitle() + "目录初始化：" + getSysFilePath("wmlFile"));
				resource.setPath(getSysFilePath("wmlFile"));
				fileResourceServiceImpl.editFileresourceEntity(resource, user);
			}
		}
		parameterServiceImpl.refreshCache();
	}

	
	private static String getSysFilePath(String dirname) {
		String tomcatPath = System.getProperty("catalina.home");
		return tomcatPath + File.separator + "resource" + File.separator + dirname;
	}

}
