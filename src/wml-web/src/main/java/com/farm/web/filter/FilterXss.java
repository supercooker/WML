package com.farm.web.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


public class FilterXss implements Filter {

	public void destroy() {
		// TODO Auto-generated method stub
	}

	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		String requestUrl = request.getRequestURL().toString();
		if (requestUrl.indexOf("/PubUpBase64File.") >= 0 || //
				requestUrl.indexOf("pointsetting/edit.") >= 0 || //
				requestUrl.indexOf("pointsetting/add.") >= 0 || //
				requestUrl.indexOf("/base64img.") >= 0 || //
				requestUrl.indexOf("pointsetting/testformula.") >= 0 || //
				requestUrl.indexOf("/api/event") >= 0) {
			// PubUpBase64File是上传base64位的截图则不继续xss校验,
			// base64img是restfulApi的base64接口
			// 事件接口不需要校验
			chain.doFilter(arg0, arg1);
			return;
		}
		chain.doFilter(new XssHttpServletRequestWrapper((HttpServletRequest) arg0), arg1);
	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	}
}