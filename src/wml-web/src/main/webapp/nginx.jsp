<%@page import="com.farm.core.auth.util.Urls"%>
<%@page import="com.farm.web.filter.utils.NginxConfig"%>
<%@page import="com.farm.parameter.service.impl.ConstantVarService"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@page import="com.farm.util.spring.BeanFactory"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>" />
<title><PF:ParameterValue key="config.sys.title" /></title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="index,follow">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="text/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="text/lib/bootstrap/css/bootstrap-theme.min.css"
	rel="stylesheet">
<script src="text/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="text/lib/bootstrap/respond.min.js"></script>
<style>
.wcp-hometypes a {
	font-size: 10px;
	margin-left: 4px;
}
</style>
</head>
<body style="padding: 20px;">
	<div style="text-align: left; font-size: 14px; margin: 20px;">
		<h1>header</h1>
		<%
		Enumeration names = request.getHeaderNames();
		while (names.hasMoreElements()) {
			Object name = names.nextElement();
		%>
		<%=name + " = " + request.getHeader((String) name) + "<br/>"%>

		<%
		}
		%>
	</div>
	<div style="text-align: left; font-size: 14px; margin: 20px;">
		<h1>nginx config</h1>
		<%="request.able = " + NginxConfig.getString("request.able",request.getServerName()) + "<br/>"%>
		<%="request.scheme = " + NginxConfig.getString("request.scheme",request.getServerName()) + "<br/>"%>
		<%="request.host = " + NginxConfig.getString("request.host",request.getServerName()) + "<br/>"%>
		<%="request.port = " + NginxConfig.getString("request.port",request.getServerName()) + "<br/>"%>
		<%="request.path = " + NginxConfig.getString("request.path",request.getServerName()) + "<br/>"%>
	</div>
	<div style="text-align: left; font-size: 14px; margin: 20px;">
		<h1>request</h1>
		<%="request.scheme = " + request.getScheme() + "<br/>"%>
		<%="request.host = " + request.getServerName() + "<br/>"%>
		<%="request.port = " + request.getServerPort() + "<br/>"%>
		<%="request.path = " + request.getContextPath() + "<br/>"%>
	</div>
	<div style="text-align: left; font-size: 14px; margin: 20px;">
		<h1>running</h1> 
		<%
		String requestUrl = request.getRequestURL().toString();
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
				+ request.getContextPath() + "/";
		String formatUrl = Urls.formatUrl(requestUrl,
				requestUrl.indexOf(":") < 8 ? basePath.replace(":80/", "/").replace(":443/", "/") : basePath,request.getContextPath());
		%>
		<%="requestUrl = " + requestUrl + "</br>"%>
		<%="basePath = " + basePath + "</br>"%>
		<%="formatUrl = " + formatUrl%>
	</div>
</body>
</html>