<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>权限资源数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',split:true,border:false"
		style="width: 250px;">
		<div class="TREE_COMMON_BOX_SPLIT_DIV">
			<a id="MessageconsoleTreeReload" href="javascript:void(0)"
				class="easyui-linkbutton" data-options="plain:true"
				iconCls="icon-reload">刷新</a> <a id="MessageconsoleTreeOpenAll"
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="plain:true" iconCls="icon-sitemap">展开</a>
		</div>
		<ul id="MessageconsoleTree"></ul>
	</div>
	<div class="easyui-layout" data-options="region:'center',border:false">
		<div data-options="region:'north',border:false">
			<form id="searchPostForm">
				<table class="editTable">
					<tr>
						<td class="title">组织机构:</td>
						<td><input id="PARENTTITLE_RULE" type="text"
							readonly="readonly" style="background: #F3F3E8"> <input
							id="PARENTID_RULE" name="PARENTID:=" type="hidden"></td>
						<td class="title">名称:</td>
						<td><input name="NAME:like" type="text" style="width: 120px;">
						</td>
						<td class="title"></td>
						<td></td>
						<td colspan="2"><a id="a_search" href="javascript:void(0)"
							class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
							id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload">清除条件</a></td>
					</tr>
				</table>
			</form>
		</div>
		<div data-options="region:'center',border:false">
			<table id="dataPostGrid">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="NAME" data-options="sortable:true" width="40">角色名称
						</th>
						<th field="EXTENDIS" data-options="sortable:true" width="20">
							作用范围</th>
						<th field="ORGNAME" data-options="sortable:true" width="40">所属机构
						</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
	var url_delActionPost = "post/del.do";//删除URL
	var url_formActionPost = "post/form.do";//增加、修改、查看URL
	var url_searchActionPost = "post/query.do";//查询URL
	var title_windowPost = "角色管理";//功能名称
	var gridPost;//数据表格对象
	var pageType = '${pageset.operateType}';
	var searchPost;//条件查询组件对象
	var toolBarPost = [ {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataPost
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataPost
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataPost
	}, {
		id : 'lock',
		text : '设置权限',
		iconCls : 'icon-lock',
		handler : setActions
	}, {
		id : 'usermng',
		text : '角色人员管理',
		iconCls : 'icon-customers',
		handler : usermng
	} ];
	$(function() {
		/* if (pageType == '0') {
			//toolBarPost = [];
		} */
		//初始化数据表格
		gridPost = $('#dataPostGrid').datagrid({
			url : url_searchActionPost,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarPost,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			striped : true,
			border : false,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchPost = $('#searchPostForm').searchForm({
			gridObj : gridPost
		});
		$('#MessageconsoleTree').tree({
			url : 'organization/organizationTree.do',
			onSelect : function(node) {
				$('#PARENTID_RULE').val(node.id);
				$('#PARENTTITLE_RULE').val(node.text);
				searchPost.dosearch({
					'ruleText' : searchPost.arrayStr()
				});
			}
		});
		$('#MessageconsoleTreeReload').bind('click', function() {
			$('#MessageconsoleTree').tree('reload');
		});
		$('#MessageconsoleTreeOpenAll').bind('click', function() {
			$('#MessageconsoleTree').tree('expandAll');
		});
	});
	//查看
	function viewDataPost() {
		var selectedArray = $(gridPost).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionPost + '?operateType=' + PAGETYPE.VIEW
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winPostForm',
				width : 600,
				height : 200,
				modal : true,
				url : url,
				title : '浏览角色'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataPost() {
		var url = url_formActionPost + '?orgid=' + $('#PARENTID_RULE').val()
		+ '&operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winPostForm',
			width : 600,
			height : 250,
			modal : true,
			url : url,
			title : '新增角色'
		});
	}
	//角色人员管理
	function usermng() {
		var selectedArray = $(gridPost).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url ='post/users.do?operateType=' + PAGETYPE.EDIT
					+ '&postid=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winUserMng',
				width : 600,
				height : 400,
				modal : true,
				url : url,
				title : '角色人员'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//修改
	function editDataPost() {
		var selectedArray = $(gridPost).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionPost + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winPostForm',
				width : 600,
				height : 250,
				modal : true,
				url : url,
				title : '修改角色'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//设置角色权限
	function setActions() {
		var selectedArray = $(gridPost).datagrid('getSelections');
		if (selectedArray.length == 1) {
			$.farm.openWindow({
				id : 'winPostActionsChoose',
				width : 300,
				height : 400,
				modal : true,
				url : "post/postActions.do?ids=" + selectedArray[0].ID,
				title : '设置权限'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//删除
	function delDataPost() {
		var selectedArray = $(gridPost).datagrid('getSelections');
		//if (selectedArray.length > 0) {
		// 有数据执行操作
		var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
		$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
			if (flag) {
				$(gridPost).datagrid('loading');
				$.post(url_delActionPost + '?ids='
						+ $.farm.getCheckedIds(gridPost, 'ID'), {}, function(
						flag) {
					var jsonObject = JSON.parse(flag, null);
					$(gridPost).datagrid('loaded');
					if (jsonObject.model.STATE == 0) {
						$(gridPost).datagrid('reload');
					} else {
						var str = MESSAGE_PLAT.ERROR_SUBMIT
								+ flag.pageset.message;
						$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
					}
				});
			}
		});
		//} else {
		//	$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
		//			'info');
		//}
	}
</script>
</html>