<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false">
		<form id="searchActionForm">
			<table class="editTable">
				<tr>
					<td class="title">名称:</td>
					<td><input name="NAME:like" type="text" style="width: 120px;">
					</td>
					<td class="title">资源KEY:</td>
					<td><input name="AUTHKEY:like" type="text"
						style="width: 120px;"></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataPostUserGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="USERNAME" data-options="sortable:true" width="30">名称
					</th>
					<th field="LOGINNAME" data-options="sortable:true" width="30">登陆名
					</th>
					<th field="POSTS" data-options="sortable:false" width="30">角色</th>
					<th field="USERSTATE" data-options="sortable:true" width="20">
						状态</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_searchActionPostUser = "post/postUserQuery.do?postid=${postid}";//查询URL
	var title_windowPostUser = "用户管理";//功能名称
	var gridPostUser;//数据表格对象
	var searchPostUser;//条件查询组件对象
	var pageType = '${pageset.operateType}';
	var toolBarPostUser = [ {
		id : 'adduser',
		text : '导入用户',
		iconCls : 'icon-customers',
		handler : addUserByOrg
	}, {
		id : 'del',
		text : '移除用户',
		iconCls : 'icon-remove',
		handler : delDataPostUser
	} ];
	$(function() {
		/*  if (pageType == '0') {
				toolBarPostUser = [];
			}  */
		//初始化数据表格
		gridPostUser = $('#dataPostUserGrid').datagrid({
			url : url_searchActionPostUser,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarPostUser,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			striped : true,
			border : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchPostUser = $('#searchPostUserForm').searchForm({
			gridObj : gridPostUser
		});
	});
	//导入用户
	function addUserByOrg() {
		var url = "user/chooseUser.do?orgid=${orgid}";
		$.farm.openWindow({
			id : 'chooseUserWin',
			width : 600,
			height : 400,
			modal : true,
			url : url,
			title : '导入用户'
		});
		chooseWindowCallBackHandle = function(row) {
			var userids;
			$(row).each(function(i, obj) {
				if (userids) {
					userids = userids + ',' + obj.ID;
				} else {
					userids = obj.ID;
				}
			});
			$(gridPostUser).datagrid('loading');
			//当前组织机构的id
			$.post("user/addUserPost.do", {
				'ids' : userids,
				postids : '${postid}'
			},
					function(flag) {
						if (flag.STATE == 0) {
							$('#chooseUserWin').window('close');
							$(gridPostUser).datagrid('reload');
						} else {
							$.messager.alert(MESSAGE_PLAT.ERROR, flag.MESSAGE,
									'error');
						}
					}, 'json');
		};
	}
	//删除
	function delDataPostUser() {
		var selectedArray = $(gridPostUser).datagrid('getSelections');
		if (selectedArray.length >= 1) {
			// 有数据执行操作
			var str = "该操作将从组织机构中移除所选用户是否继续？";
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridPostUser).datagrid('loading');
					$.post('user/delUserPost.do?postids=${postid}&ids='
							+ $.farm.getCheckedIds(gridPostUser, 'USERID'), {},
							function(flag) {
								$(gridPostUser).datagrid('loaded');
								var jsonObject = JSON.parse(flag, null);
								if (jsonObject.STATE == 0) {
									$(gridPostUser).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>