<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<PF:basePath/>">
    <title>附件功能日志数据管理</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <jsp:include page="/view/conf/include.jsp"></jsp:include>
  </head>
  <body class="easyui-layout">
    <div data-options="region:'north',border:false">
      <form id="searchVisitlogsForm">
        <table class="editTable">
          <tr>
            <td class="title">
              TYPE:
            </td>
            <td>
        <select name="TYPE:like">
          <option value="0">NONE</option>
        </select>
            </td>
            <td class="title"></td>
            <td></td>
          </tr>
          <tr style="text-align: center;">
            <td colspan="4">
              <a id="a_search" href="javascript:void(0)"
                class="easyui-linkbutton" iconCls="icon-search">查询</a>
              <a id="a_reset" href="javascript:void(0)"
                class="easyui-linkbutton" iconCls="icon-reload">清除条件</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div data-options="region:'center',border:false">
      <table id="dataVisitlogsGrid">
        <thead>
          <tr>
            <th data-options="field:'ck',checkbox:true"></th>
            <th field="STRFLAG" data-options="sortable:true" width="70">STRFLAG</th>
            <th field="INTFLAG" data-options="sortable:true" width="70">INTFLAG</th>
            <th field="TYPE" data-options="sortable:true" width="40">TYPE</th>
            <th field="CUSER" data-options="sortable:true" width="50">CUSER</th>
            <th field="CTIME" data-options="sortable:true" width="50">CTIME</th>
            <th field="ID" data-options="sortable:true" width="20">ID</th>
          </tr>
        </thead>
      </table>
    </div>
  </body>
  <script type="text/javascript">
    var url_delActionVisitlogs = "visitlogs/del.do";//删除URL
    var url_formActionVisitlogs = "visitlogs/form.do";//增加、修改、查看URL
    var url_searchActionVisitlogs = "visitlogs/query.do";//查询URL
    var title_windowVisitlogs = "附件功能日志管理";//功能名称
    var gridVisitlogs;//数据表格对象
    var searchVisitlogs;//条件查询组件对象
    var toolBarVisitlogs = [ {
      id : 'view',
      text : '查看',
      iconCls : 'icon-tip',
      handler : viewDataVisitlogs
    }, {
      id : 'add',
      text : '新增',
      iconCls : 'icon-add',
      handler : addDataVisitlogs
    }, {
      id : 'edit',
      text : '修改',
      iconCls : 'icon-edit',
      handler : editDataVisitlogs
    }, {
      id : 'del',
      text : '删除',
      iconCls : 'icon-remove',
      handler : delDataVisitlogs
    } ];
    $(function() {
      //初始化数据表格
      gridVisitlogs = $('#dataVisitlogsGrid').datagrid( {
        url : url_searchActionVisitlogs,
        fit : true,
        fitColumns : true,
        'toolbar' : toolBarVisitlogs,
        pagination : true,
        closable : true,
        checkOnSelect : true,
        border:false,
        striped : true,
        rownumbers : true,
        ctrlSelect : true
      });
      //初始化条件查询
      searchVisitlogs = $('#searchVisitlogsForm').searchForm( {
        gridObj : gridVisitlogs
      });
    });
    //查看
    function viewDataVisitlogs() {
      var selectedArray = $(gridVisitlogs).datagrid('getSelections');
      if (selectedArray.length == 1) {
        var url = url_formActionVisitlogs + '?pageset.pageType='+PAGETYPE.VIEW+'&ids='
            + selectedArray[0].ID;
        $.farm.openWindow( {
          id : 'winVisitlogs',
          width : 600,
          height : 300,
          modal : true,
          url : url,
          title : '浏览'
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
            'info');
      }
    }
    //新增
    function addDataVisitlogs() {
      var url = url_formActionVisitlogs + '?operateType='+PAGETYPE.ADD;
      $.farm.openWindow( {
        id : 'winVisitlogs',
        width : 600,
        height : 300,
        modal : true,
        url : url,
        title : '新增'
      });
    }
    //修改
    function editDataVisitlogs() {
      var selectedArray = $(gridVisitlogs).datagrid('getSelections');
      if (selectedArray.length == 1) {
        var url = url_formActionVisitlogs + '?operateType='+PAGETYPE.EDIT+ '&ids=' + selectedArray[0].ID;
        $.farm.openWindow( {
          id : 'winVisitlogs',
          width : 600,
          height : 300,
          modal : true,
          url : url,
          title : '修改'
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
            'info');
      }
    }
    //删除
    function delDataVisitlogs() {
      var selectedArray = $(gridVisitlogs).datagrid('getSelections');
      if (selectedArray.length > 0) {
        // 有数据执行操作
        var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
        $.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
          if (flag) {
            $(gridVisitlogs).datagrid('loading');
            $.post(url_delActionVisitlogs + '?ids=' + $.farm.getCheckedIds(gridVisitlogs,'ID'), {},
              function(flag) {
                var jsonObject = JSON.parse(flag, null);
                $(gridVisitlogs).datagrid('loaded');
                if (jsonObject.STATE == 0) {
                  $(gridVisitlogs).datagrid('reload');
              } else {
                var str = MESSAGE_PLAT.ERROR_SUBMIT
                    + jsonObject.MESSAGE;
                $.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
              }
            });
          }
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
            'info');
      }
    }
  </script>
</html>