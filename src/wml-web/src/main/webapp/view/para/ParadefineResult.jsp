<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>参数定义数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchParadefineForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td class="title">名称:</td>
					<td><input name="NAME:like" type="text"></td>
					<td class="title">KEY:</td>
					<td><input name="PKEY:like" type="text"></td>
					<td><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataParadefineGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="40">名称</th>
					<th field="PKEY" data-options="sortable:true" width="40">PKEY</th>
					<th field="NOTE" data-options="sortable:true" width="40">注释</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionParadefine = "paradefine/del.do";//删除URL
	var url_formActionParadefine = "paradefine/form.do";//增加、修改、查看URL
	var url_searchActionParadefine = "paradefine/query.do";//查询URL
	var title_windowParadefine = "参数定义管理";//功能名称
	var gridParadefine;//数据表格对象
	var searchParadefine;//条件查询组件对象
	var toolBarParadefine = [ //{
	//id : 'view',
	//text : '查看',
	//iconCls : 'icon-tip',
	//	handler : viewDataParadefine
	//},
	{
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataParadefine
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataParadefine
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataParadefine
	} ];
	$(function() {
		//初始化数据表格
		gridParadefine = $('#dataParadefineGrid').datagrid({
			url : url_searchActionParadefine,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarParadefine,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchParadefine = $('#searchParadefineForm').searchForm({
			gridObj : gridParadefine
		});
	});
	//查看
	function viewDataParadefine() {
		var selectedArray = $(gridParadefine).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionParadefine + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winParadefine',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataParadefine() {
		var url = url_formActionParadefine + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winParadefine',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataParadefine() {
		var selectedArray = $(gridParadefine).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionParadefine + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winParadefine',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataParadefine() {
		var selectedArray = $(gridParadefine).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridParadefine).datagrid('loading');
					$.post(url_delActionParadefine + '?ids='
							+ $.farm.getCheckedIds(gridParadefine, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridParadefine).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridParadefine).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>