<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>参数组数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchParagroupForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td class="title">名称:</td>
					<td><input name="NAME:like" type="text"></td>
					<td class="title">KEY:</td>
					<td><input name="GROUPKEY:like" type="text"></td>
					<td colspan="2"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataParagroupGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="40">组名称</th>
					<th field="GROUPKEY" data-options="sortable:true" width="40">组KEY</th>
					<th field="NUM" data-options="sortable:false" width="40">参数量</th> 
					<th field="STATE" data-options="sortable:true" width="40">状态</th>
					<th field="COMMENTS" data-options="sortable:true" width="80">备注</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionParagroup = "paragroup/del.do";//删除URL
	var url_formActionParagroup = "paragroup/form.do";//增加、修改、查看URL
	var url_searchActionParagroup = "paragroup/query.do";//查询URL
	var title_windowParagroup = "参数组管理";//功能名称
	var gridParagroup;//数据表格对象
	var searchParagroup;//条件查询组件对象
	var toolBarParagroup = [//{
	//id : 'view',
	//text : '查看',
	//iconCls : 'icon-tip',
	//handler : viewDataParagroup
	//}, 
	{
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataParagroup
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataParagroup
	}, {
		id : 'copy',
		text : '复制',
		iconCls : 'icon-administrative-docs',
		handler : copyDataParagroup
	}, {
		id : 'paras',
		text : '设置参数',
		iconCls : 'icon-issue',
		handler : editDataParas
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataParagroup
	} ];
	$(function() {
		//初始化数据表格
		gridParagroup = $('#dataParagroupGrid').datagrid({
			url : url_searchActionParagroup,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarParagroup,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchParagroup = $('#searchParagroupForm').searchForm({
			gridObj : gridParagroup
		});
	});
	//查看
	function viewDataParagroup() {
		var selectedArray = $(gridParagroup).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionParagroup + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winParagroup',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataParagroup() {
		var url = url_formActionParagroup + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winParagroup',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//复制
	function copyDataParagroup() {
		var selectedArray = $(gridParagroup).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + "个参数组将被复制是否继续?";
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridParagroup).datagrid('loading');
					$.post('paragroup/copy.do?ids='
							+ $.farm.getCheckedIds(gridParagroup, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridParagroup).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridParagroup).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
	//修改
	function editDataParagroup() {
		var selectedArray = $(gridParagroup).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionParagroup + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winParagroup',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//修改组参数
	function editDataParas() {
		var selectedArray = $(gridParagroup).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = 'paragrouppara/list.do?groupid=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winParagroup',
				width : 700,
				height : 400,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//删除
	function delDataParagroup() {
		var selectedArray = $(gridParagroup).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridParagroup).datagrid('loading');
					$.post(url_delActionParagroup + '?ids='
							+ $.farm.getCheckedIds(gridParagroup, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridParagroup).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridParagroup).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>