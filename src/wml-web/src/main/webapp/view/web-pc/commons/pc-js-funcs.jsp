<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<script>
	var wml_current_info_key = "none";

	function clickWmlCategray(categrayId) {
		if (!categrayId) {
			categrayId = $('#currentCategrayId').val();
		}
		$('#wml—right-infoboxid').html('loading...');
		$('#wml—right-infoboxid').load('ui/infoCategray.do?id=' + categrayId);
		if (categrayId && categrayId != "NONE") {
			selectTreeNode(categrayId);
		}
		loadFiles('ui/categrayfiles.do?categrayid=' + categrayId);
	}

	function loadWmlCategrayInfo(categrayId) {
		if (categrayId && wml_current_info_key != categrayId) {
			wml_current_info_key = categrayId;
			$('.wml-ui-filsbox .light').removeClass('active');
			$('.' + categrayId).addClass('active');
			$('#wml—right-infoboxid').html('loading...');
			$('#wml—right-infoboxid').load(
					'ui/infoCategray.do?id=' + categrayId, {}, function() {
						$('#wml—right-infoboxid').hide();
						$('#wml—right-infoboxid').fadeIn(1000);
					});
		}
	}

	function loadWmlFileInfo(fileId) {
		if (topmenu_win_type == 'SELECT') {
			$('.wml-ui-filsbox .light').removeClass('active');
			//多选模式
			if ($('.' + fileId).hasClass('mactive')) {
				$('.' + fileId).removeClass('mactive');
			} else {
				$('.' + fileId).addClass('mactive');
			}
		} else {
			//单选模式
			if (fileId && wml_current_info_key != fileId) {
				wml_current_info_key = fileId;
				$('.wml-ui-filsbox .light').removeClass('active');
				$('.' + fileId).addClass('active');
				$('#wml—right-infoboxid').html('loading...');
				$('#wml—right-infoboxid').load(
						'ui/infoFile.do?appId=' + fileId, {}, function() {
							$('#wml—right-infoboxid').hide();
							$('#wml—right-infoboxid').fadeIn(1000);
						});
			}
		}
	}
	function dblclickFile(title, id, modellabel, exname) {
		if (modellabel == 'IMG') {
			showImg('download/Pubimg.do?fileid=' + id);
			return;
		}
		if (exname == 'mp4') {
			wmlplayMp4(title, 'download/Pubload.do?id=' + id);
			return;
		}
		if (exname == 'mp3') {
			wmlplayMp3(title, 'download/Pubload.do?id=' + id);
			return;
		}
		if (exname == 'pdf') {
			wmlOpenIframe('download/Pubload.do?id=' + id);
			return;
		}
		{
			wmlConfirm('立即下载文件<'+title+'>吗?', 'download/Pubfile.do?id=' + id,
					'link')
		}
	}
	function initTopButtons(catagaryKey) {
		if (catagaryKey == "NONE" || catagaryKey == "FAVORITES"
				|| catagaryKey == "BOOK" || catagaryKey == "VISIT"
				|| catagaryKey == "RECYCLE") {
			closeTopMenuWin();
			topmenu_win_type = 'NONE';
			$('#wmlTopButtonUploadId').hide();
			$('#wmlTopButtonSelectId').hide();
		}else{
			$('#wmlTopButtonUploadId').show();
			$('#wmlTopButtonSelectId').show();
		}
	}
</script>
<script>
	function loadTrees(val) {
		$('#data').jstree(true).destroy();
		$('#data').on("ready.jstree", function(e, data) {
			if (val) {
				$('#data').jstree(true).open_all();
			}
		}).on("select_node.jstree", function(e, data) {
			clickWmlCategray(data.selected)
		}).jstree({
			'core' : {
				'data' : {
					"url" : "ui/categrays.do?name=" + val,
					"dataType" : "json" // needed only if you do not supply JSON headers
				}
			}
		});
	}
	var topmenu_win_type;//UPLOAD,
	var topmenu_win_stat;//OPEN,CLOSE

	function openTopMenuWin() {
		topmenu_win_stat = 'OPEN';
		wml_ui_innerCenter.open("north");
		initCenterWinSize();
	}
	function closeTopMenuWin() {
		topmenu_win_stat = 'CLOSE';
		wml_ui_innerCenter.close("north");
		initCenterWinSize();
	}
	function initCenterWinSize() {
		$('#wml_ui_categray_files')
				.height(
						$(document).height()
								- $('#wml_ui_categray_files').offset().top);
	}
</script>
<!-- 附件相关 -->
<script>
	function addFileNode(imgUrl, fileName, fileId, domBoxId) {
		var categrayid = $('#currentCategrayId').val();
		if (!categrayid) {
			alert("请选择一个文件分类!");
		} else {
			$.post('webfile/submitfile.do', {
				'fileid' : fileId,
				'categrayid' : categrayid
			}, function(jsonObject) {
				if (jsonObject.STATE == 0) {
					$('#wml_ui_newfile_fresh_id').show();
					$('#wml_ui_newfile_fresh_id').attr(
							'data-num',
							1 + parseInt($('#wml_ui_newfile_fresh_id').attr(
									'data-num')));
					$('#wml_ui_newfile_fresh_num_id').text(
							$('#wml_ui_newfile_fresh_id').attr('data-num'));
				} else {
					alert(jsonObject.MESSAGE);
				}
			}, 'json');
		}
	}
	function refreshFilesByWmlUi() {
		$('#wml_ui_newfile_fresh_id').attr('data-num', 0);
		$('#wml_ui_newfile_fresh_id').hide();
		clickWmlCategray();
	}
</script>