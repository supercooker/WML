<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<jsp:include page="pc-userinfo.jsp"></jsp:include>
<div id="wml—right-infoboxid" class="wml—right-infobox"
	style="margin: 20px;">
	<h4>基本信息</h4>
	<table style="width: 100%;">
		<tr>
			<td style="width: 100px;">名称:</td>
			<td><PF:ParameterValue key="config.sys.title" /></td>
		</tr>
		<tr>
			<td style="width: 100px;">当前用户id:</td>
			<td
				style="white-space: normal; word-wrap: break-word; word-break: break-all;">${USEROBJ.id}</td>
		</tr>
	</table>
	<c:if test="${!empty resources }">
		<h4>资源信息</h4>
		<table style="width: 100%;">
			<c:forEach items="${resources }" var="node">
				<tr>
					<td style="width: 100px;">${node.title}:</td>
					<td
						style="white-space: normal; word-wrap: break-word; word-break: break-all;">

						<div class="progress">
							<div class="progress-bar" role="progressbar" title="${node.spaceinfo}"
								style="width:  ${node.spacenum}%;" aria-valuenow="25"
								aria-valuemin="0" aria-valuemax="100">${node.spacenum}%</div>
						</div>
					</td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
</div>