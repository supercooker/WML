<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<style>
body {
	background-color: #000000;
}

img {
	-webkit-user-drag: none;
}

.ui-layout-resizer-west-open {
	background-color: #000000;
}

.ui-layout-resizer-east-open {
	background-color: #000000;
}

.wml-ui-l1-center {
	background-color: #303134;
	color: #b9bbbd;
}

.wml-ui-l1-east {
	background-color: #353639;
	color: #b9bbbd;
}

.wml-ui-l1-west {
	background-color: #353639;
	color: #b9bbbd;
}

.wml-ui-l2-center-top {
	background-color: #353639;
	color: #b9bbbd;
}

.wml-pc-body hr {
	border: #444548 solid 1px;
	margin-top: 20px;
	margin-bottom: 20px;
}

.wml-pc-body a {
	color: #b9bbbd;
}

.wml-pc-body a:hover {
	color: #ffffff;
}

.wml-ui-left-menus {
	padding: 10px;
	font-size: 16px;
}

.wml-ui-left-menus .menus-node {
	padding-left: 0px;
	font-size: 16px;
	margin-top: 8px;
	margin-bottom: 2px;
}

.menus-node a {
	color: #b9bbbd;
	padding: 4px;
	border: 1px solid;
	border-color: transparent;
	cursor: pointer;
}

.menus-node a:hover {
	text-decoration: none;
	background-color: #444548;
	padding: 4px;
	border: 1px solid #6e6f71;
	border-radius: 3px;
}

.wml-ui-dir-search .form-control {
	background-color: #303134;
	border: 1px solid #000000;
	color: #b9bbbd;
}

.wml-ui-dir-search .input-group-text {
	background-color: #303134;
	border: 1px solid #000000;
	color: #b9bbbd;
}

.wml-ui-dir-search .input-group-addon {
	color: #888888;
	background-color: #303134;
	border: 1px solid #000000;
	border-radius: 4px 4px;
}

.wml—right-infobox {
	font-size: 12px;
}

.wml—right-infobox h4 {
	color: #eeeeee;
	font-size: 14px;
	margin-top: 20px;
}

.wml-ui-tags .tag {
	float: left;
	padding: 4px;
	text-decoration: none;
	background-color: #444548;
	padding: 4px;
	border: 1px solid #6e6f71;
	border-radius: 3px;
	margin: 4px;
	margin-left: 0px;
	padding-top: 2px;
	padding-bottom: 2px;
	padding-top: 2px;
}

.btn-secondary {
	color: #fff;
	background-color: #444548;
	border: 1px dashed #6e6f71;
}

.btn-secondary:hover {
	color: #d7d7d7;
	background-color: #535457;
}

.jstree-node a i {
	font-size: 14px;
	margin-left: 10px;
	color: #6c757d;
}

.wml-ui-tags .tag:hover {
	color: #d7d7d7;
	background-color: #535457;
}

.wml_ui_plusWin {
	font-size: 14px;
	user-select: none;
}

.row .light {
	padding-left: 2px;
	padding-right: 2px;
}
</style>
