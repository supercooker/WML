<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- 批量上传附件 -->
<script src="text/lib/fileUpload/jquery.ui.widget.js"></script>
<script src="text/lib/fileUpload/jquery.iframe-transport.js"></script>
<script src="text/lib/fileUpload/jquery.fileupload.js"></script>
<link href="text/lib/fileUpload/jquery.fileupload.css" rel="stylesheet">
<style>
.dropFiles {
	border: 2px dashed #545558;
	height: 80px;
	margin: 10px;
	background-color: #444548;
	text-align: center;
	padding-top: 15px;
	margin: 10px;
	background-color: #444548;
	text-align: center;
	background-color: #444548;
	text-align: center;
	text-align: center;
	overflow: hidden;
}

.dropFiles:hover {
	border: 2px dashed #000000;
	background-color: #545558;
}

.fileOverActive {
	border: 2px dashed #000000;
	background-color: #545558;
}

.fileinput-button {
	display: inline-block;
	padding-top: 0px;
	padding-bottom: 4px;
	cursor: pointer;
}
</style>
<div class="dropFiles WfsUploadDropBox" id="uploadModalContentId"
	style="text-align: center;">
	<div class="media" style="width: 390px; margin: auto;">
		<i style="font-size: 32px;" class="bi bi-cloud-upload mr-3"></i>
		<div class="media-body" style="padding-top: 10px;">
			&nbsp;将文件<b>拖入此框</b>内或<b>点击按钮</b>&nbsp;&nbsp; <span
				class="btn btn-light fileinput-button" role="button"> <span><i
					class="bi bi-cloud-upload"></i>&nbsp;批量上传文件</span> <span
				id="html5uploadProsess"></span> <input id="fileupload" type="file"
				name="file" multiple>
			</span>
			<div class="progress" id="fileUploadProgressBarId"
				style="margin-left: 0px; margin-right: 0px; margin: 4px; display: none;">
				<div id="progressCpercentTitleId" class="progress-bar"
					role="progressbar" aria-valuenow="60" aria-valuemin="0"
					aria-valuemax="100" style="width: 0%;">0%</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	//单个文件大小
	var maxSize = parseInt("${config_doc_upload_length_max}");
	var maxSizeTitle = (maxSize / 1024 / 1024).toFixed(2);
	var currentUserId='${USEROBJ.id}';
	//单次上传文件数量
	var maxFileNum = 30;
	$(function() {
		 document.addEventListener('drop', function (e) {
			 e.preventDefault()
		 }, false)
			  document.addEventListener('dragover', function (e) {
			 e.preventDefault()
		}, false);
		$('#fileupload')
			.fileupload(
					{
						url : "upload/general.do",
						dataType : 'json',
						change : function(e, data) {
							if(!(e.delegatedEvent.cancelable)){
								//禁止拖拽上传
								return updateFileValidate(data.files);
							}else{
								return false;
							}
						}, 
						submit:function(e,data){
							if(!(e.delegatedEvent.cancelable)){
								//禁止拖拽上传
								return updateFileValidate(data.files);
							}else{
								return false;
							}
						},
						done : function(e, data) {
							var url = data.result.url;
							var state = data.result.STATE;
							var message = data.result.MESSAGE;
							var id = data.result.id;
							var fileName = data.result.fileName;
							if (state == 1) {
								showErrorMessage(message)
								return;
							}
							addFileNode('download/Pubicon.do?id=' + id,
									decodeURIComponent(fileName), id);
						},
						progressall : function(e, data) {
							var progress = parseInt(data.loaded
									/ data.total * 100, 10);
							loadFileProcess("上传",progress,'client');
							if (progress == 100) {
								setFileProcessBar(false);
								loadRemoteProcess(true);
							} else {
								setFileProcessBar(true);
							}
						}
					});
		initDropBox();
	});
	
	function initDropBox(){
		var uploadDrop = document.getElementById('uploadModalContentId');
		uploadDrop.addEventListener('dragover', function(event) {
			$('.WfsUploadDropBox').addClass("fileOverActive");
			event.preventDefault();
		});
		uploadDrop.addEventListener('dragenter', function(event) {
			event.preventDefault();
		});
		uploadDrop.addEventListener('dragleave', function(event) {
			$('.WfsUploadDropBox').removeClass("fileOverActive");
			event.preventDefault();
		});
		uploadDrop.addEventListener('drop', function(event) {
			$('.WfsUploadDropBox').removeClass("fileOverActive");
			event.preventDefault();
			var fileList = Array.from(event.dataTransfer.files);
			var uploadAble = updateFileValidate(fileList);
			if (uploadAble) {
				for (var i = 0; i < fileList.length; i++) {
					sendFileByXHR('upload/general.do', fileList[i]);
				}
			}
		});
	}
	
	function updateFileValidate(files) {
		if (files.length > maxFileNum) {
			alert("单次上传文件数量不能大于" + maxFileNum + "个!");
			return false;
		}
		var isOK = true;
		$(files).each(function(i, obj) {
			if (obj.size > maxSize) {
				alert("文件" + obj.name + "超大,请检查文件大小不能大于" + maxSizeTitle + "m");
				isOK = false;
			}
		});
		{
			var categrayid = $('#currentCategrayId').val();
			if (!categrayid) {
				alert("请选择一个文件分类!");
				isOK = false;
			}
		}
		return isOK;
	}
	function loadFileProcess(title,num,type){
		var allnum=0;
		if(type=='server'){
			allnum=50+num/2; 
		}
		if(type=='client'){
			allnum=num/2;
		}
		$('#progressCpercentTitleId').text(title+":"+allnum + '%');
		$('#progressCpercentTitleId').css('width',allnum+'%');
	}
	function setFileProcessBar(flag){
		if(flag){
			$('#fileUploadProgressBarId').show();
		}else{
			$('#fileUploadProgressBarId').hide();
		}
	}
	
	function loadRemoteProcess(state){
		isLoadRemotProecess=state;
		if(state){
			setFileProcessBar(true);
			setTimeout("doloadRemoteProcess()",1000); //延迟1秒
		}
	}
	function doloadRemoteProcess(){
		$.post("upload/PubUploadProcess.do",{},function(flag){ 
			if(flag.STATE==0){
				loadFileProcess("服务器端处理文件",flag.process,'server');
				if(flag.process>99){
					isLoadRemotProecess=false;
					setFileProcessBar(false);
				}
				if(isLoadRemotProecess){
					setTimeout("doloadRemoteProcess()",1000); //延迟1秒	
				}
			}else{
				alert(flag.MESSAGE+"#001");
			} 
		},'json');
	}
	function sendFileByXHR(url, fielObj) {
		var xhr = new XMLHttpRequest();
		xhr.upload.onprogress = function(event) {
			var progress = parseInt(event.loaded / event.total * 100, 10);
			setFileProcessBar(true)
			if (progress == 100) {
				setFileProcessBar(false);
				loadRemoteProcess(true);
			} else {
				setFileProcessBar(true);
			}
			loadFileProcess("上传",progress,'client');
		}
		xhr.onreadystatechange = function() {
			if (xhr.status === 200 && xhr.readyState === 4) {
				var jsonObject = JSON.parse(xhr.responseText, null);
				var url = jsonObject.url;
				var state = jsonObject.STATE;
				var message = jsonObject.MESSAGE;
				var id = jsonObject.id;
				var fileName = jsonObject.fileName;
				if (state == 1) {
					showErrorMessage(message)
					return;
				}
				addFileNode('download/Pubicon.do?id=' + id,
						decodeURIComponent(fileName), id);
			}
		}
		xhr.onabort = function(event) {
			//终止
			console.log('abort');
		}
		xhr.onerror = function(event) {
			console.log('error');
			showErrorMessage('xhr.onerror-error');
		}
		var data = new FormData();
		data.append("file", fielObj);
		xhr.open('POST', url, true);
		if(!updateFileValidate(data)){
			return;
		}
		xhr.send(data);
	}
	//顯示错误消息弹框
	function showErrorMessage(message) {
		alert(message+"#003");
	}
	
	//添加一个界面文件元素
	//function addFileNode(imgUrl, fileName, fileId, domBoxId) {
	//	alert(fileName+"#002");
	//}
</script>