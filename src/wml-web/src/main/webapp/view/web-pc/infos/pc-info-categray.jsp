<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${!empty categray }">
	<div style="text-align: center;">
		<div class="btn-toolbar" role="toolbar"
			style="margin: auto; margin-top: 8px; text-align: center;"
			aria-label="Toolbar with button groups">
			<div class="btn-group mr-2 btn-group-sm" role="group"
				aria-label="First group">
				<c:if test="${!empty vlogid}">
					<a type="button" class="btn btn-secondary"
						onclick="wmlConfirm('确认取消订阅<${file.file.title}>吗?','opfile/delvlog.do?vlogid=${vlogid}','ajax','freshFiles')">取消订阅</a>
				</c:if>
				<c:if test="${empty vlogid}">
					<a type="button" class="btn btn-secondary"
						onclick="wmlConfirm('确认订阅分类<${categray.name}>吗?','opfile/book.do?categrayId=${categray.id}','ajax','Tip')">订阅分类</a>
				</c:if>
			</div>
		</div>
	</div>
</c:if>
<c:if test="${!empty categray }">
	<h4>基本信息</h4>
	<table style="width: 100%;">
		<tr>
			<td style="width: 100px;">名称:</td>
			<td>${categray.name}</td>
		</tr>
		<tr>
			<td style="width: 100px;">描述:</td>
			<td>${categray.pcontent}</td>
		</tr>
	</table>
	<h4>文件信息</h4>
	<table style="width: 100%;">
		<tr>
			<td style="width: 100px;">当前文件数量:</td>
			<td>${categray.cnum}</td>
		</tr>
		<tr>
			<td style="width: 100px;">全部文件数量:</td>
			<td>${categray.allnum}</td>
		</tr>
	</table>
</c:if>
<c:if test="${empty categray }">
	<h4>基本信息</h4>
	<table style="width: 100%;">
		<tr>
			<td style="width: 100px;">名称:</td>
			<td><c:if test="${empty title }">
					<PF:ParameterValue key="config.sys.title" />
				</c:if> 
				<c:if test="${!empty title }">${title}</c:if></td>
		</tr>
		<tr>
			<td style="width: 100px;">当前用户id:</td>
			<td
				style="white-space: normal; word-wrap: break-word; word-break: break-all;">${USEROBJ.id}</td>
		</tr>
	</table>
</c:if>
<c:if test="${!empty tags }">
	<h4>可用标签</h4>
	<div class="wml-ui-tags">
		<c:forEach items="${tags }" var="node">
			<div class="tag">${node.name }</div>
		</c:forEach>
	</div>
	<div style="clear: both;"></div>
</c:if>
<c:if test="${!empty resources }">
	<h4>可用资源</h4>
	<table style="width: 100%;">
		<c:forEach items="${resources }" var="node">
			<tr>
				<td style="width: 100px;">${node.title}:</td>
				<td
					style="white-space: normal; word-wrap: break-word; word-break: break-all;">

					<div class="progress">
						<div class="progress-bar" role="progressbar"
							title="${node.spaceinfo}" style="width:  ${node.spacenum}%;"
							aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">${node.spacenum}%</div>
					</div>
				</td>
			</tr>
		</c:forEach>
	</table>
</c:if>
<c:if test="${!empty visit}">
	<jsp:include page="pc-info-file-praise.jsp"></jsp:include>
</c:if>
<c:if test="${!empty visit}">
	<jsp:include page="pc-info-file-comments.jsp"></jsp:include>
</c:if>

<script type="text/javascript">
	$(function() {
		wml_current_info_key = '${id}';
	})
</script>