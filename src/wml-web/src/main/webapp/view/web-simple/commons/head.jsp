<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<style>
.messgeNum .badge {
	margin-top: -5px;
	background-color: #d13133;
	font-weight: 400;
} 
</style>
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header" style="padding-left: 40px;">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<c:if test="${empty sysid }">
				<a class="navbar-brand" href="<PF:defaultIndexPage/>"
					style="padding: 5px; padding-top: 7px;"> <img
					class="img-rounded" src="<PF:basePath/>webfile/Publogo.do"
					height="40" alt="WLP" align="middle" /></a>
			</c:if>
			<c:if test="${!empty sysid }">
				<a class="navbar-brand" style="padding: 5px; padding-top: 7px;">
					<c:if test="${empty syslogo }">
						<img class="img-rounded" src="<PF:basePath/>webfile/Publogo.do"
							height="40" alt="WLP" align="middle" />
					</c:if> <c:if test="${!empty syslogo }">
						<img class="img-rounded" src="${syslogo}" height="40" alt="WLP"
							align="middle" />
					</c:if>
				</a>
			</c:if>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<!-- <ul class="nav navbar-nav">
				<li><a href="<PF:defaultIndexPage/>">首页<span
						class="sr-only">(current)</span></a></li>
			</ul>  -->
			<ul class="nav navbar-nav navbar-right">
				<c:if test="${USEROBJ==null&&empty type&&empty sysid }">
					<li><a href="login/webPage.do"><i
							class="glyphicon glyphicon-log-in"></i>&nbsp;用户登陆</a></li>
					<!-- 	<li><a>/</a></li>
					<li><a href="#"><i
							class="glyphicon glyphicon-registration-mark"></i>&nbsp;注冊</a></li> -->
				</c:if>
				<c:if test="${USEROBJ!=null&&empty sysid }">
					<li role="separator" class="divider"></li>
					<li class="dropdown"><a class="dropdown-toggle"
						style="min-width: 160px;" data-toggle="dropdown" role="button"
						aria-haspopup="true" aria-expanded="false"> <img alt=""
							src="download/PubPhoto.do?id=${USEROBJ.imgid}"
							style="width: 24px; height: 24px; position: relative; top: -2px;"
							class="img-circle">&nbsp;${USEROBJ.name }&nbsp;<span
							class="messgeNum"></span> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<!-- <li><a href="#">我的空间</a></li> -->
							<c:if test="${USEROBJ.type=='3'}">
								<li class="hidden-xs hidden-sm"><a href="frame/index.do"><i
										class="glyphicon glyphicon-wrench"></i>&nbsp;管理控制台</a></li>
							</c:if>
							<c:if test="${USEROBJ.type=='3'}">
								<li><a href="userspace/settinginfo.do"><i
										class="glyphicon glyphicon-cog"></i>&nbsp;信息设置</a></li>
							</c:if>
							<c:if test="${USEROBJ.type!='3'}">
								<PF:IfParameterEquals key="config.useredit.reception.able"
									val="true">
									<li><a href="userspace/settinginfo.do"><i
											class="glyphicon glyphicon-cog"></i>&nbsp;信息设置</a></li>
								</PF:IfParameterEquals>
							</c:if>
							<li role="separator" class="divider"></li>
							<li><a href="login/webout.do"><i
									class="glyphicon glyphicon-log-out"></i>&nbsp;注销</a></li>
						</ul></li>
				</c:if>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid -->
</nav>
<script type="text/javascript">
  //后退url相关操作
	$(function() {
		setBackUrl();
		//如果没有返回外部url就不显示返回按钮
		if (!hasBackUrl()) {
			$('.wucBackButton').hide();
		}
	});

	function gotoBackUrl() {
		if (getCookieBakcUrl()) {
			document.location.href = getCookieBakcUrl();
		}
		delCookieBackUrl();
	}
	function hasBackUrl() {
		if (getCookieBakcUrl()) {
			return true;
		}
		return false;
	}
	function setBackUrl() {
		var curl = document.referrer;
		if (curl.indexOf("userspace/settinginfo.") >= 0) {
			return;
		}
		if (curl.indexOf("userspace/settingpsd.") >= 0) {
			return;
		}
		if (curl.indexOf("userspace/settingcontact.") >= 0) {
			return;
		}
		if (curl.indexOf("userpoint/infos.") >= 0) {
			return;
		}
		if (curl.indexOf("userpoint/infos.") >= 0) {
			return;
		}
		if (curl.indexOf("msg/infos.") >= 0) {
			return;
		}
		if (curl.indexOf("msg/view.") >= 0) {
			return;
		}
		setCookieBackUrl(curl);
	}
	function delCookieBackUrl() {
		var name = "wuc-back-url";
		var exp = new Date();
		exp.setTime(exp.getTime() - 1);
		var cval = getCookieBakcUrl(name);
		if (cval != null)
			document.cookie = name + "=" + cval + ";expires="
					+ exp.toGMTString();
	}
	function setCookieBackUrl(value) {
		var name = "wuc-back-url";
		var Days = 30;
		var exp = new Date();
		exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
		document.cookie = name + "=" + escape(value);
	}

	//读cookie
	function getCookieBakcUrl() {
		var name = "wuc-back-url";
		var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
		if (arr = document.cookie.match(reg))
			return unescape(arr[2]);
		else
			return null;
	}
</script>