<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="index,follow">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<jsp:include page="atext/include-web.jsp"></jsp:include>
<style>
<!--
.wuc-userinfo th {
	min-width: 100px;
}
-->
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<!-- 导航 -->
			<jsp:include page="commons/head.jsp"></jsp:include>
		</div>
	</div>
	<div class="container-fluid "
		style="background-color: #eceef1; padding-top: 80px; padding-bottom: 50px;">
		<div class="container" style="margin-bottom: 50px;">
			<div class="row">
				<div class="col-md-12" style="padding-bottom: 20px;">
					<jsp:include page="commons/curentUserInfo.jsp"></jsp:include>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<a href="ui/index.do">进入素材库</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid" style="padding: 0px;">
		<!-- 页脚 -->
		<jsp:include page="commons/foot.jsp"></jsp:include>
	</div>
</body>
</html>