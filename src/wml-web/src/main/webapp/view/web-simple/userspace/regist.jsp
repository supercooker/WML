<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<title>注册-<PF:ParameterValue key="config.sys.title" />
</title>
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="noindex,nofllow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
<script src="text/javascript/base64.js"></script>
<script src="text/javascript/md5.js"></script>
<script src="text/javascript/encode.provider.js"></script>
<script src="text/javascript/jssha256.js"></script>
<style>
.alertMsgClass, .errorMsgClass {
	color: #D9534F;
	font-size: 12px;
	margin: 4px;
}
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<div class="containerbox">
		<div class="container ">
			<div class="row" style="margin-top: 70px;">
				<div class="col-sm-3  visible-lg visible-md"></div>
				<div class="col-sm-9">
					<div class="row">
						<div class="col-sm-12" style="margin-bottom: 8px;">
							<span style="color: #D9534F;" class="glyphicon glyphicon-user   ">用户注册</span>
							<hr />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<c:if test="${pageset.commitType=='1'}">
								<div class="alert alert-danger">${pageset.message}</div>
							</c:if>
							<form role="form" action="userspace/PubRegistCommit.do"
								id="registSubmitFormId" method="post">
								<div class="row">
									<div class="col-xs-6">
										<div class="form-group">
											<label for="exampleInputEmail1"> 登录账户 <span
												class="alertMsgClass">*</span>
											</label>
											<div class="row">
												<div class="col-md-9">
													<input type="text" class="form-control" name="loginname"
														id="loginnameId" placeholder="输入账户名称" value="${loginname}" />
												</div>
												<div class="col-md-3"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-6">
										<div class="form-group">
											<label for="exampleInputEmail1"> 姓名 <span
												class="alertMsgClass">*</span>
											</label>
											<div class="row">
												<div class="col-md-9">
													<input type="text" class="form-control" name="name"
														id="nameid" placeholder="输入真实姓名" value="${name}" />
												</div>
												<div class="col-md-3"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-6">
										<div class="form-group">
											<label for="exampleInputEmail1"> 登录密码 <span
												class="alertMsgClass">*</span>
											</label>
											<div class="row">
												<div class="col-md-9">
													<input type="password" id="passwordId" class="form-control"
														name="password" placeholder="输入登录密码" />
												</div>
												<div class="col-md-3"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-6">
										<div class="form-group">
											<label for="exampleInputEmail1"> 重复密码 <span
												class="alertMsgClass">*</span>
											</label>
											<div class="row">
												<div class="col-md-9">
													<input type="password" id="passwordId2"
														class="form-control" placeholder="重新输入登录密码" />
												</div>
												<div class="col-md-3"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<div class="form-group" id="loginCheckFormId">
											<label for="exampleInputEmail1">验证码<span
												class="alertMsgClass">*</span></label>
											<div class="input-group">
												<input type="text" class="form-control" placeholder="请录入验证码"
													id="checkcodeId" name="checkcode">
												<div class="input-group-addon" style="padding: 0px;">
													<img id="checkcodeimgId"
														style="cursor: pointer; height: 30px; width: 100px;"
														src="webfile/Pubcheckcode.do" />
												</div>
											</div>
										</div>
										<span id="checkcodeMes"></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<button type="button" id="registSubmitButtonId"
											class="btn btn-primary btn-lg">提交</button>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="errorMsgClass " id="errormessageShowboxId">${errorMessage }</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<br /> <br /> <br /> <br />
		</div>
	</div>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
	<script type="text/javascript">
		$(function() {
			$('#registSubmitButtonId')
					.bind(
							'click',
							function() {
								if (!validate('registSubmitFormId')) {
									$('#errormessageShowboxId').text(
											'信息录入有误，请检查！');
								} else {
									if (confirm("是否提交注册信息?") == true) {
										var scret_password = (AuthKeyProvider
												.encodeEdit(
														'${config_password_provider_type}',
														$('#passwordId').val(),
														$('#loginnameId').val(),
														'${sck}'));
										$('#passwordId').val(scret_password);
										$('#registSubmitFormId').submit();
									}
								}
							});
			validateInput('loginnameId', function(id, val, obj) {
				// 登录名
				if (valid_isNull(val)) {
					return {
						valid : false,
						msg : '不能为空'
					};
				}
				if (!valid_isLoginname(val)) {
					return {
						valid : false,
						msg : '只能使用英文字母或数字'
					};
				}
				if (!valid_maxLength(val, 3 - 1)) {
					return {
						valid : false,
						msg : '不能小于3个字符'
					};
				}
				if (valid_maxLength(val, 16)) {
					return {
						valid : false,
						msg : '不能大于16个字符'
					};
				}
				return {
					valid : true,
					msg : '正确'
				};
			});
			validateInput('nameid', function(id, val, obj) {
				// 用户名
				if (valid_isNull(val)) {
					return {
						valid : false,
						msg : '不能为空'
					};
				}
				if (!valid_maxLength(val, 2 - 1)) {
					return {
						valid : false,
						msg : '不能小于2个字符'
					};
				}
				if (valid_maxLength(val, 16)) {
					return {
						valid : false,
						msg : '不能大于16个字符'
					};
				}
				return {
					valid : true,
					msg : '正确'
				};
			});

			validateInput('checkcodeId', function(id, val, obj) {
				// 验证码
				if (valid_isNull(val)) {
					return {
						valid : false,
						msg : '不能为空'
					};
				}
				return {
					valid : true,
					msg : '正确'
				};
			}, 'checkcodeMes');

			validateInput('passwordId', function(id, val, obj) {
				// 密码
				if (valid_isNull(val)) {
					return {
						valid : false,
						msg : '不能为空'
					};
				}
				if (!valid_maxLength(val, 6 - 1)) {
					return {
						valid : false,
						msg : '不能小于6个字符'
					};
				}
				if (valid_maxLength(val, 32)) {
					return {
						valid : false,
						msg : '不能大于32个字符'
					};
				}
				try {
					var regex = new RegExp(
							/${config_sys_password_update_regex}/);
					if (!regex.test(val)) {
						return {
							valid : false,
							msg : '${config_sys_password_update_tip}'
						};
					}
				} catch (e) {
					//正则表达式验证失败
					alert(e);
				}
				return {
					valid : true,
					msg : '正确'
				};
			});
			validateInput('passwordId2', function(id, val, obj) {
				// 重录密码
				if (valid_isNull(val)) {
					return {
						valid : false,
						msg : '不能为空'
					};
				}
				if ($('#passwordId').val() != $('#passwordId2').val()) {
					return {
						valid : false,
						msg : '两次密码输入不一样'
					};
				}
				return {
					valid : true,
					msg : '正确'
				};
			});
			validateInput('emailid', function(id, val, obj) {
				if (valid_isNull(val)) {
					return {
						valid : false,
						msg : '不能为空'
					};
				}
				if (valid_maxLength(val, 32)) {
					return {
						valid : false,
						msg : '长度不能大于' + 32
					};
				}
				if (!isEmail(val)) {
					return {
						valid : false,
						msg : '格式不正确'
					};
				}
				return {
					valid : true,
					msg : '正确'
				};
			});
			$('#checkcodeimgId').bind(
					"click",
					function(e) {
						$('#checkcodeimgId').attr(
								"src",
								"webfile/Pubcheckcode.do?time="
										+ new Date().getTime());
					});
		});
	</script>
</body>
</html>